# mysql user provisioner

This is a simple python module that provisions mysql users based on a yaml config file

## how to use

The minimal python code to use this module is:

```python
import mysql_user_provisioner

provisioner = mysql_user_provisioner.Provisioner("user.yml")
provisioner.provision_users()
```

The code expects a config file called `user.yml` in the same directory. You can 

**Beware: Based on the config file it will start making changes on the mysql server. 
Consider using `debug: True` in your connection first.**

## config file format

The config file must be compliant with the yaml standard (see pyyaml library for details).

The config contains of root two objects, `users` and `connection`.

### users

Users are provided as list and need at least these properties to get created:

* `username`
* `password`

Users that have no password field are assumed as existent and only get permissions granted.

Permissions can be a single string or a list of strings, e.g. `SELECT` as a single string.

User object reference:

| key          | description                                     | allowed types | required             | default |
|--------------|-------------------------------------------------|---------------|----------------------|---------|
| username     | name for the user                               | string        | yes                  | None    |
| password     | password for the user                           | string        | for user creation    | None    |
| permissions  | permissions for the user                        | string, list  | to grant permissions | None    |
| grant_option | add grant option to user permssions permissions | string, list  | to grant permissions | None    |
| tables       | restrict permissions to tables                  | string        | no                   | None    | 
| hostname     | restrict  user client ip/hostname               | string        | no                   | "%"     |
| update       | update user if already exists                   | bool          | no                   | False   |
| delete       | delete user if already exists                   | bool          | no                   | False   |
| ignore       | ignore user                                     | bool          | no                   | False   |

## connection

The connection object ist a dictionary that describes connection parameters for the mysql server.

You have to at least provide `username` and `password` to connect to a local mysql server.

Connection object reference:

| key           | description                                          | allowed types | required | default     |
|---------------|------------------------------------------------------|---------------|----------|-------------|
| username      | name of for the server connection                    | string        | yes      | None        |
| password      | password for the server connection                   | string        | yes      | None        |
| hostname      | hostname for the server                              | string        | no       | "localhost" |
| debug         | print everything to stdout instead of sql connection | bool          | no       | False       | 
| log_passwords | print passwords in debug outputs                     | bool          | no       | False       |