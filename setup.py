from setuptools import setup

setup(
    name='mysql-user-provisioner',
    version='0.3.0',
    packages=['mysql_user_provisioner'],
)
