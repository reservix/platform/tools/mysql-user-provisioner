import logging

from .config import Config
from .connection import Connection

logger = logging.getLogger("mysql-user-provisioner")
logging.basicConfig(level=logging.INFO)


class Provisioner:

    def __init__(self, config_file=None, config_string=None, no_log=False):
        if config_file is None and config_string is None:
            raise TypeError("requires at least one of these arguments: config_file, config_string")
        if config_string is not None:
            self.config = Config(config_string=config_string, no_log=no_log)
        else:
            self.config = Config(config_file=config_file, no_log=no_log)

        connection_config = self.config.connection
        if connection_config is None:
            raise Exception("connection config not available")
        self.connection = Connection(connection_config)

    def provision_users(self):
        for user_dict in self.config.users:
            if user_dict.get("ignore"):
                continue
            if user_dict.get("delete"):
                self.connection.delete_user(username=user_dict.get("username"),
                                            hostname=user_dict.get("hostname"))
            elif user_dict.get("password"):
                self.create_user(username=user_dict.get("username"),
                                 password=user_dict.get("password"),
                                 permissions=user_dict.get("permissions"),
                                 tables=user_dict.get("tables"),
                                 hostname=user_dict.get("hostname"),
                                 update=user_dict.get("update"),
                                 grant_option=user_dict.get("grant_option"))
            else:
                self.grant_permissions(username=user_dict.get("username"),
                                       permissions=user_dict.get("permissions"),
                                       tables=user_dict.get("tables"),
                                       hostname=user_dict.get("hostname"),
                                       grant_option=user_dict.get("grant_option"))
        self.connection.commit()
        return self.connection.history

    def create_user(self, username, password, permissions=None, tables=None, hostname=None, update=None, grant_option=False):
        tables = '*.*' if tables is None else tables
        hostname = '%' if hostname is None else hostname
        update = False if update is None else update

        self.connection.create_user(username, password, hostname, update)
        if permissions is not None:
            self._grant_permissions(username, permissions, tables, hostname, grant_option)
        self.connection.flush_privileges()

    def grant_permissions(self, username, permissions, tables, hostname=None, grant_option=False):
        tables = '*.*' if tables is None else tables
        hostname = '%' if hostname is None else hostname
        self._grant_permissions(username, permissions, tables, hostname, grant_option),
        self.connection.flush_privileges()

    def _grant_permissions(self, username, permissions, tables, hostname, grant_option=False):
        if type(permissions) is list:
            for perm in permissions:
                self.connection.grant_permission(username, perm, tables, hostname, grant_option)
        else:
            self.connection.grant_permission(username, permissions, tables, hostname, grant_option)
