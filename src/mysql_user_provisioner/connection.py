import logging
import re

import pymysql.cursors

logger = logging.getLogger("mysql-user-provisioner")


class Connection:

    def __init__(self, connection_config):
        self._connection = None
        self._print_debugging = connection_config.get("debug") is True
        self._log_passwords = connection_config.get("log_passwords") is True
        self.history = []

        if self._print_debugging:
            return

        self._connection = pymysql.connect(host=connection_config.get("hostname"),
                                           user=connection_config.get("username"),
                                           passwd=connection_config.get("password"),
                                           db=connection_config.get("database"))
        self._cursor = self._connection.cursor()

    def commit(self):
        if self._connection is not None:
            self._connection.commit()

    def check_user_exists(self, username):
        query = f"SELECT * FROM mysql.user WHERE user = '{username}';"
        result_cursor = self.query(query)
        if result_cursor is not None:
            return result_cursor.rowcount > 0
        return False

    def create_user(self, username, password, hostname, update=False):
        if self.check_user_exists(username):
            if not update:
                raise Exception("user already exists")
            return self.update_user(username, password, hostname)
        query = f"CREATE USER '{username}'@'{hostname}' IDENTIFIED BY '{password}';"
        self.query(query)

    def update_user(self, username, password, hostname):
        query = f"ALTER USER '{username}'@'{hostname}' IDENTIFIED BY '{password}';"
        return self.query(query)

    def delete_user(self, username, hostname=None):
        if not self.check_user_exists(username):
            raise Exception("user does not exists")

        return self._delete_user(username, hostname)

    def delete_user_if_exists(self, username, hostname=None):
        return self._delete_user(username, hostname)

    def _delete_user(self, username, hostname=None):
        hostname = '%' if hostname is None else hostname

        query = f"DROP USER IF EXISTS '{username}'@'{hostname}';"
        return self.query(query)

    def grant_permission(self, username, permission, tables, hostname, grant_option=False):
        query = f"GRANT {permission} ON {tables} TO '{username}'@'{hostname}'{' WITH GRANT OPTION' if grant_option else ''};"
        self.query(query)

    def flush_privileges(self):
        self.query("FLUSH PRIVILEGES;")

    def query(self, query):
        if len(query.split(";")) <= 0:
            raise Exception("No semicolon in statement")

        self.history.append(query)

        if self._print_debugging:
            if "identified by" in query.lower() and not self._log_passwords:
                query = re.sub(r'identified by (\'(.*)\')', "IDENTIFIED BY '****'", query, flags=re.IGNORECASE)
            logger.debug(query)
        else:
            self._cursor.execute(query)
            return self._cursor

    def __del__(self):
        if self._print_debugging:
            return

        if self._connection is not None:
            self._connection.close()
