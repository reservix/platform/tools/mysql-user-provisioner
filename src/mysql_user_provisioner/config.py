import os
import logging
import pprint
import yaml

logger = logging.getLogger("mysql-user-provisioner")


class Config:
    def __init__(self, config_file="user.yml", config_string=None, no_log=False):
        if no_log:
            logger.setLevel(logging.FATAL)

        if config_string is not None:
            logger.info("reading config from string...")
            self._yaml_to_config(config_string)
        else:
            if not os.path.isabs(config_file):
                import sys
                config_file = os.path.join(os.path.dirname(sys.argv[0]), config_file)
            logger.info(f"reading config file: {config_file}")
            with open(config_file, "r") as stream:
                self._yaml_to_config(stream)
        logger.info(f"Your config looks like this\n{self}")
        if self.debug and not no_log:
            logger.setLevel(logging.DEBUG)

        self._check_config_parameters()

    def _check_config_parameters(self):
        if not all(k in self.config for k in ("users", "connection")):
            raise Exception("You need at least a users and a connection object in your config")
        if not all(k in self.connection for k in ("username", "password")):
            raise Exception("username and password must be set for the connection")

    def _yaml_to_config(self, yaml_object):
        try:
            self.config = yaml.safe_load(yaml_object)
        except yaml.YAMLError as exc:
            logger.error(exc)

    def _config_without_passwords(self):
        return {k: self._replace_passwords(k, v) for k, v in self.config.items()}

    def _replace_passwords(self, key, value):
        if self.connection.get("log_passwords") is True:
            return value

        if type(value) is dict:
            return {k: self._replace_passwords(k, v) for k, v in value.items()}
        elif type(value) is list:
            return [self._replace_passwords(None, v) for v in value]
        elif key is None:
            pass
        elif type(value) is str:
            if "pass" in key.lower():
                return "*****"
        return value

    @property
    def connection(self):
        return self.config.get("connection")

    @property
    def users(self):
        return self.config.get("users")

    @property
    def debug(self):
        return self.connection.get("debug") is True

    @property
    def log_passwords(self):
        return self.connection.get("log_passwords") is True

    def get(self, key):
        return self.config.get(key)

    def __getitem__(self, key):
        return self.config[key]

    def __str__(self):

        return pprint.pformat(self._config_without_passwords(), width=200)
