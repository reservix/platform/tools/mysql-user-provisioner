import mysql_user_provisioner
import yaml
import unittest

no_log = False


class TestConfig(unittest.TestCase):
    mysql_server = None

    def test_config_dict(self):
        config_data = """
            users: 
              - username: root
                password: "Zava$ij7Kia1iephi!" 
            connection:
              username: root
              password: insecure
        """
        expected_config = yaml.safe_load(config_data)
        self.assertDictEqual(expected_config,
                             mysql_user_provisioner.Config(config_string=config_data, no_log=no_log).config)

    def test_missing_connection_parameters(self):
        config_data = """
            users: 
              - username: root
                password: "Zava$ij7Kia1iephi!" 
            connection:
              username: root
        """
        self.assertRaises(Exception, mysql_user_provisioner.Config, config_string=config_data, no_log=no_log)
        config_data = """
            users: 
              - username: root
                password: "Zava$ij7Kia1iephi!" 
            connection:
              password: root
        """
        self.assertRaises(Exception, mysql_user_provisioner.Config, config_string=config_data, no_log=no_log)

    def test_user_creation(self):
        config_data = """
            users: 
              - username: root
                password: "Zava$ij7Kia1iephi!"
              - username: dba
                password: "Chiec6eemae7joh3!" 
            connection:
              username: root
              password: "eu5OhGhie6ay8Ke1"
              debug: True
        """
        expected_result = [
            "SELECT * FROM mysql.user WHERE user = 'root';",
            "CREATE USER 'root'@'%' IDENTIFIED BY 'Zava$ij7Kia1iephi!';",
            "FLUSH PRIVILEGES;",
            "SELECT * FROM mysql.user WHERE user = 'dba';",
            "CREATE USER 'dba'@'%' IDENTIFIED BY 'Chiec6eemae7joh3!';",
            "FLUSH PRIVILEGES;",
        ]
        computed_result = mysql_user_provisioner.Provisioner(config_string=config_data, no_log=no_log).provision_users()
        self.assertListEqual(expected_result, computed_result)

    def test_grant_permission(self):
        config_data = """
            users: 
              - username: root
                permissions: "ALL"
                tables: "products.*"
              - username: dba
                password: "Chiec6eemae7joh3!"
                permissions: "ALL"
                hostname: "10.4.128.%"
            connection:
              username: root
              password: "eu5OhGhie6ay8Ke1"
              debug: True
        """
        expected_result = [
            "GRANT ALL ON products.* TO 'root'@'%';",
            "FLUSH PRIVILEGES;",
            "SELECT * FROM mysql.user WHERE user = 'dba';",
            "CREATE USER 'dba'@'10.4.128.%' IDENTIFIED BY 'Chiec6eemae7joh3!';",
            "GRANT ALL ON *.* TO 'dba'@'10.4.128.%';",
            "FLUSH PRIVILEGES;",
        ]
        computed_result = mysql_user_provisioner.Provisioner(config_string=config_data, no_log=no_log).provision_users()
        self.assertListEqual(expected_result, computed_result)

    def test_grant_permission_with_grant_option(self):
        config_data = """
            users: 
              - username: root
                permissions: "ALL"
                tables: "products.*"
              - username: dba
                password: "Chiec6eemae7joh3!"
                permissions: "ALL"
                hostname: "10.4.128.%"
                grant_option: True
              - username: dba
                permissions: "SELECT"
                hostname: "10.4.128.%"
                grant_option: True
            connection:
              username: root
              password: "eu5OhGhie6ay8Ke1"
              debug: True
        """
        expected_result = [
            "GRANT ALL ON products.* TO 'root'@'%';",
            "FLUSH PRIVILEGES;",
            "SELECT * FROM mysql.user WHERE user = 'dba';",
            "CREATE USER 'dba'@'10.4.128.%' IDENTIFIED BY 'Chiec6eemae7joh3!';",
            "GRANT ALL ON *.* TO 'dba'@'10.4.128.%' WITH GRANT OPTION;",
            "FLUSH PRIVILEGES;",
            "GRANT SELECT ON *.* TO 'dba'@'10.4.128.%' WITH GRANT OPTION;",
            "FLUSH PRIVILEGES;",
        ]
        computed_result = mysql_user_provisioner.Provisioner(config_string=config_data, no_log=no_log).provision_users()
        self.assertListEqual(expected_result, computed_result)


    def test_real_connection(self):
        if self.mysql_server is None:
            self.skipTest("MYSQL_SERVER not set")
        config_data = f"""
            users: 
              - username: root
                permissions: "ALL"
                tables: "products.*"
              - username: dba
                password: "Chiec6eemae7joh3!"
                permissions: "ALL"
                hostname: "10.4.128.%"
            connection:
              username: root
              password: "eu5OhGhie6ay8Ke1"
              hostname: {mysql_server}
        """
        expected_result = [
            "GRANT ALL ON products.* TO 'root'@'%';",
            "FLUSH PRIVILEGES;",
            "SELECT * FROM mysql.user WHERE user = 'dba';",
            "CREATE USER 'dba'@'10.4.128.%' IDENTIFIED BY 'Chiec6eemae7joh3!';",
            "GRANT ALL ON *.* TO 'dba'@'10.4.128.%';",
            "FLUSH PRIVILEGES;",
        ]
        provisioner = mysql_user_provisioner.Provisioner(config_string=config_data, no_log=no_log)
        provisioner.connection.delete_user_if_exists("dba", "10.4.128.%")
        provisioner.connection.delete_user_if_exists("dba")
        provisioner.connection.history = []
        self.assertListEqual(expected_result, provisioner.provision_users())

    def test_delete_user(self):
        if self.mysql_server is None:
            self.skipTest("MYSQL_SERVER not set")
        config_data = f"""
            users: 
              - username: root
                permissions: "ALL"
                tables: "products.*"
              - username: dba
                hostname: "10.4.128.%"
                delete: True
            connection:
              username: root
              password: "eu5OhGhie6ay8Ke1"
              hostname: {mysql_server}
        """
        expected_result = [
            "GRANT ALL ON products.* TO 'root'@'%';",
            "FLUSH PRIVILEGES;",
            "SELECT * FROM mysql.user WHERE user = 'dba';",
            "DROP USER IF EXISTS 'dba'@'10.4.128.%';",
        ]
        provisioner = mysql_user_provisioner.Provisioner(config_string=config_data, no_log=no_log)
        provisioner.connection.delete_user_if_exists("dba", "10.4.128.%")
        provisioner.connection.delete_user_if_exists("dba")
        provisioner.connection.create_user("dba", "inscure", "10.4.128.%")
        provisioner.connection.history = []
        self.assertListEqual(expected_result, provisioner.provision_users())

    def test_update_user(self):
        if self.mysql_server is None:
            self.skipTest("MYSQL_SERVER not set")
        config_data = f"""
            users: 
              - username: root
                permissions: "ALL"
                tables: "products.*"
              - username: dba
                password: "Chiec6eemae7joh3!"
                permissions: "ALL"
                hostname: "10.4.128.%"
                update: True
            connection:
              username: root
              password: "eu5OhGhie6ay8Ke1"
              hostname: {mysql_server}
        """
        expected_result = [
            "GRANT ALL ON products.* TO 'root'@'%';",
            "FLUSH PRIVILEGES;",
            "SELECT * FROM mysql.user WHERE user = 'dba';",
            "ALTER USER 'dba'@'10.4.128.%' IDENTIFIED BY 'Chiec6eemae7joh3!';",
            "GRANT ALL ON *.* TO 'dba'@'10.4.128.%';",
            "FLUSH PRIVILEGES;",
        ]
        provisioner = mysql_user_provisioner.Provisioner(config_string=config_data, no_log=no_log)
        provisioner.connection.delete_user_if_exists("dba", "10.4.128.%")
        provisioner.connection.delete_user_if_exists("dba")
        provisioner.connection.create_user("dba", "insecure", "10.4.128.%")
        provisioner.connection.history = []
        self.assertListEqual(expected_result, provisioner.provision_users())

    def test_ignore_user(self):
        config_data = f"""
            users: 
              - username: root
                password: "Chiec6eemae7joh3!"
                ignore: True
              - username: root
                permissions: "ALL"
                hostname: "10.4.128.%"
                ignore: True
              - username: root
                ignore: True
              - username: dba
                password: "Chiec6eemae7joh3!"
                permissions: "ALL"
                hostname: "10.4.128.%"
            connection:
              username: root
              password: "eu5OhGhie6ay8Ke1"
              debug: True
        """
        expected_result = [
            "SELECT * FROM mysql.user WHERE user = 'dba';",
            "CREATE USER 'dba'@'10.4.128.%' IDENTIFIED BY 'Chiec6eemae7joh3!';",
            "GRANT ALL ON *.* TO 'dba'@'10.4.128.%';",
            "FLUSH PRIVILEGES;",
        ]
        provisioner = mysql_user_provisioner.Provisioner(config_string=config_data, no_log=no_log)
        self.assertListEqual(expected_result, provisioner.provision_users())


if __name__ == '__main__':
    import os
    mysql_server = os.environ.get('MYSQL_SERVER')
    if mysql_server is not None:
        TestConfig.mysql_server = mysql_server
    unittest.main(verbosity=2)
